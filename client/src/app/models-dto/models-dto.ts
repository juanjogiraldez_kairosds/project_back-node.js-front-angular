

export interface PostDTO {
    _id?: string;
    author?: string;
    nickName?: string;
    title?: string;
    text?: string;
    comments?: CommentDTO[];
}

export interface CommentDTO {
    _id?: string;
    nickName?: string;
    comment?: string;
    createdAt?: Date;

}
