import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { TokenDTO } from './access-dto';

@Injectable({
  providedIn: 'root'
})
export class LoginProxyService {


  constructor(private httpClient: HttpClient) { }

  loginUser(username: string, password: string): Observable<TokenDTO> {

    const auth = btoa(`${username}:${password}`);
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json', Authorization: 'Basic ' + auth })
    };

    return this.httpClient.post<TokenDTO>('http://localhost:3000/api/login', '', httpOptions)
      ;
  }
}

