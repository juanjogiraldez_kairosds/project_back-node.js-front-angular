import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationBusService } from 'src/app/back-office/business/notification-bus.service';
import { LoginService } from './login.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  usernameErrorsMessages ;


  constructor(private loginService: LoginService, private router: Router,
              private notify: NotificationBusService) { }

  ngOnInit(): void {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required, Validators.minLength(4)]), // ,  [Validators.required, Validators.minLength(4)])
      password: new FormControl('', [Validators.required, Validators.minLength(4)]) // ,  [Validators.required, Validators.minLength(4)])
    });
    this.usernameErrorsMessages = {
      required: 'Este campo es requerido',
      minlength: 'añade minimo 4 caracteres',
    };

  }

  login() {
    const username = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    this.loginService.loginUser(username, password);
  }


  goToSignUp() {
    this.router.navigate(['signUp']);
  }


}
