import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationBusService } from 'src/app/back-office/business/notification-bus.service';
import { TokenDTO } from './access-dto';
import { LoginProxyService } from './login-proxy.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private proxy: LoginProxyService,
              private router: Router,
              private notify: NotificationBusService) { }

  loginUser(username: string, password: string): void {
    this.proxy.loginUser(username, password).subscribe(
      (tokenDTO: TokenDTO) => {
        localStorage.setItem('token', tokenDTO.token);
        console.log('Has iniciado sesión ', username);
        this.router.navigateByUrl('BackOffice');
        this.notify.showSuccess(username);
    }, // mandamos el token al localStorege y seguimos a la route 'BackOffice'
      (error) =>  this.notify.showError(error.statusText)
    );

  }
}
