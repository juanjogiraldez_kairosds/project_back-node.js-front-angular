export interface UserDTO {
    user: string;
    passwordHash: string;
    role: string;

}
