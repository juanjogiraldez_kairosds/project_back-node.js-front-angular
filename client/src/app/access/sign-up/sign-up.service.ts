import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { SignUpProxyService } from './sign-up-Proxy.service';

@Injectable({
  providedIn: 'root'
})
export class SignUpService {

  constructor(private proxy: SignUpProxyService) { }

  getAddUser(data: object): Observable<any>{
    return this.proxy.getAddUser(data) ;
}
}
