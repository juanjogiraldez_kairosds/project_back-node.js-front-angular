import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class SignUpProxyService {

  constructor(private httpClient: HttpClient) { }

  getAddUser(data): Observable<any> {
    return this.httpClient.post<any>('http://localhost:3000/api/user', data)
      ;
  }
}
