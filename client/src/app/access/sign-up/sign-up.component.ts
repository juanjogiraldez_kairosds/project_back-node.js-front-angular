import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { SignUpService } from './sign-up.service';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {

  signUpForm: FormGroup;
  signUpErrorsMessages;

  constructor(private singUpService: SignUpService,  private _location: Location) { }


  ngOnInit(): void {
    this.signUpForm = new FormGroup({
      user: new FormControl('', [Validators.required, Validators.minLength(4)]),
      passwordHash: new FormControl('', [Validators.required, Validators.minLength(4)]),
      role: new FormControl('', [Validators.required])
    });
    this.signUpErrorsMessages = {
      required: 'Este campo es requerido',
      minlength: 'añade minimo 4 caracteres',
    };
  }

  signUp(){
    this.singUpService.getAddUser(this.signUpForm.value).subscribe((e) => {console.log('estas registrado'); } );
    this._location.back();
  }
}
