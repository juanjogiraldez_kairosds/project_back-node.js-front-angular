import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ErrorMessagesModule } from '../extra/error-messages/error-messages.module';
import { AccessMainComponent } from './access-main/access-main.component';
import { LoginComponent } from './login/login.component';
import { SignUpComponent } from './sign-up/sign-up.component';


const ROUTES: Routes = [
  {
    path: '', component: AccessMainComponent,
    children: [{ path: '', component: LoginComponent },
    { path: 'signUp', component: SignUpComponent }]
  }


];

@NgModule({
  declarations: [AccessMainComponent, LoginComponent, SignUpComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    ErrorMessagesModule,
    HttpClientModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [
    AccessMainComponent, LoginComponent, SignUpComponent
  ],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AccessModule { }
