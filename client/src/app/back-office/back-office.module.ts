import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import { ButtonModule } from 'primeng/button';
import { DialogModule } from 'primeng/dialog';
import { InputTextModule } from 'primeng/inputtext';
import { InputTextareaModule } from 'primeng/inputtextarea';
import { PanelModule } from 'primeng/panel';
import { TableModule } from 'primeng/table';
import { AuthService } from '../auth.service';
import { ErrorMessagesModule } from '../extra/error-messages/error-messages.module';
import { BackOfficeCreateCommentComponent } from './back-office-create-comment/back-office-create-comment.component';
import { BackOfficeCreatePostComponent } from './back-office-create-post/back-office-create-post.component';
import { BackOfficeModifyCommentComponent } from './back-office-modify-comment/back-office-modify-comment.component';
import { BackOfficePostDetailComponent } from './back-office-post-detail/back-office-post-detail.component';
import { BackOfficePostModifyComponent } from './back-office-post-modify/back-office-post-modify.component';
import { BackOfficePostComponent } from './back-office-post/back-office-post.component';
import { BackOfficeViewComponent } from './back-office-view/back-office-view.component';

const ROUTES: Routes = [
  {
    path: '', component: BackOfficeViewComponent,
    children: [
      { path: '', component: BackOfficePostComponent, canActivate: [AuthService] },
      { path: 'detail/:id', component: BackOfficePostDetailComponent, canActivate: [AuthService] },
      // { path: 'modify/:id', component: BackOfficePostModifyComponent, canActivate: [AuthService] },
      // { path: 'createPost', component: BackOfficeCreatePostComponent, canActivate: [AuthService] },
      // { path: 'createComment/:id', component: BackOfficeCreateCommentComponent, canActivate: [AuthService] },
      { path: 'modifyComment/:id', component: BackOfficeModifyCommentComponent, canActivate: [AuthService] }

    ]
  }
];

@NgModule({
  declarations: [
    BackOfficePostDetailComponent,
    BackOfficePostComponent,
    BackOfficeViewComponent,
    BackOfficePostModifyComponent,
    BackOfficeCreatePostComponent,
    BackOfficeModifyCommentComponent,
    BackOfficeCreateCommentComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ErrorMessagesModule,
    ButtonModule,
    InputTextModule,
    InputTextareaModule,
    TableModule,
    DialogModule,
    PanelModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [
    BackOfficeViewComponent,
    BackOfficePostComponent,
    BackOfficePostDetailComponent,
    BackOfficePostModifyComponent,
    BackOfficeCreatePostComponent,
    BackOfficeModifyCommentComponent],

  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class BackOfficeModule { }
