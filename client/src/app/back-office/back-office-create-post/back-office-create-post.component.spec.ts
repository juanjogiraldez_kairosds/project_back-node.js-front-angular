import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficeCreatePostComponent } from './back-office-create-post.component';

describe('BackOfficeCreatePostComponent', () => {
  let component: BackOfficeCreatePostComponent;
  let fixture: ComponentFixture<BackOfficeCreatePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficeCreatePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeCreatePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
