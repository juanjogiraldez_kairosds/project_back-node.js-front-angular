import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { NotificationBusService } from '../business/notification-bus.service';
import { PostsStore } from '../posts.store';

@Component({
  selector: 'app-back-office-create-post',
  templateUrl: './back-office-create-post.component.html',
  styleUrls: ['./back-office-create-post.component.css']
})
export class BackOfficeCreatePostComponent implements OnInit {

  @Input() entrada: boolean;
  @Output() salida = new EventEmitter<boolean>();
  createPost: FormGroup;
  error;


  constructor(private createService: PostsStore,
              private bus: NotificationBusService) { }

  ngOnInit(): void {
    this.createPost = new FormGroup({
      author: new FormControl(''),
      nickName: new FormControl(''),
      title: new FormControl(''),
      text: new FormControl('')
    });
  }

  newPost() {
    const body = this.createPost.value;
    this.createService.newPost$(body)
      .then(() => {
        this.bus.showSuccess('Se ha creado una nueva Entrada de un Post');
      })
      .catch(
        err => {
          this.error = err.error.message;
          this.bus.showError('No se ha podido crear un Post', 'Error: ');
        });
    this.createPost.reset();
    this.salida.emit(false);
  }

  showCloseThisModal() {
    this.createPost.reset();
    this.salida.emit(false);
  }

}
