import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Observable } from 'rxjs/internal/Observable';
import { NotificationBusService } from '../business/notification-bus.service';
import { Post } from '../business/post.model';
import { PostsStore } from '../posts.store';


@Component({
  selector: 'app-back-office-post',
  templateUrl: './back-office-post.component.html',
  styleUrls: ['./back-office-post.component.css']
})
export class BackOfficePostComponent implements OnInit {

  displayModal: boolean;
  displayModalModify: boolean;
  postList$: Observable<Post[]>;
  post: Post;
  item: string;
  createPost: FormGroup;
  postToModify;
  error;


  constructor(private service: PostsStore,
              private bus: NotificationBusService) { }

  ngOnInit(): void {
    this.service.init();
    this.postList$ = this.service.get$();
    this.postToModify = this.post;
    this.createPost = new FormGroup({
      author: new FormControl(''),
      nickName: new FormControl(''),
      title: new FormControl(''),
      text: new FormControl('')
    });
  }

  showCreatePost() {
    this.displayModal = true;
  }
  showCloseCreatePost(data: boolean) {
    this.displayModal = data;
  }

  showModifyPost(post: Post) {
    this.postToModify = post;
    this.displayModalModify = true;
  }

  showCloseModifyPost(data: boolean) {
    this.displayModalModify  = data;
  }

  deleteItemPost(item) {
    this.service.deleteItemPost$(item._id)
    .then(() => {
      this.bus.showSuccess('Se ha eliminado correctamente esta entrada con exito');
    })
    .catch(
      err => {
        this.error = err.error.message;
        this.bus.showError('No se ha podido eliminar esta entrada, no estas autorizado', `esto es Error ${this.error} `);
      });
  }
}
