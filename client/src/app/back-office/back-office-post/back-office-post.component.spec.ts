import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficePostComponent } from './back-office-post.component';

describe('BackOfficePostComponent', () => {
  let component: BackOfficePostComponent;
  let fixture: ComponentFixture<BackOfficePostComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficePostComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficePostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
