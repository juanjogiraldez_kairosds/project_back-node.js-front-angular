import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationBusService } from '../business/notification-bus.service';
import { CommentsStore } from '../comments.store';

@Component({
  selector: 'app-back-office-modify-comment',
  templateUrl: './back-office-modify-comment.component.html',
  styleUrls: ['./back-office-modify-comment.component.css']
})
export class BackOfficeModifyCommentComponent implements OnInit {

  @Input() entrada: boolean;
  @Input() inputModify: any;
  @Output() salida = new EventEmitter<boolean>();
  modifyComment: FormGroup;
  modifyCommentErrorsMessages;
  error;

  constructor(
    private modifyCommentService: CommentsStore,
    private bus: NotificationBusService) { }

  ngOnInit(): void {
    this.modifyComment = new FormGroup({
      nickName: new FormControl('', [Validators.required]),
      comment: new FormControl('', [Validators.required])
    });
    this.modifyCommentErrorsMessages = {
      required: 'Este campo es requerido'
    };
  }

  showCloseThisModal() {
    this.modifyComment.reset();
    this.salida.emit(false);
  }

  // con este metodo modificamos cada comment
  modify(id: string) {
    const comment = this.modifyComment.value;
    this.modifyCommentService.modifyComment$(id, comment)
    .then(() => {
      this.bus.showSuccess('Se ha modificado este comentario con exito');
    })
    .catch(
      err => {
        this.error = err.error.message;
        this.bus.showError('No se ha podido modificar este comentario , No te esta permitido modificar');
      });
    this.salida.emit(false);
  }
}
