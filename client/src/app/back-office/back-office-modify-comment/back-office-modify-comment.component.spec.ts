import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficeModifyCommentComponent } from './back-office-modify-comment.component';

describe('BackOfficeModifyCommentComponent', () => {
  let component: BackOfficeModifyCommentComponent;
  let fixture: ComponentFixture<BackOfficeModifyCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficeModifyCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeModifyCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
