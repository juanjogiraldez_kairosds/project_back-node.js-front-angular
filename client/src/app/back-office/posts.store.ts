import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { BackOfficePostService } from './business/back-office-post.service';
import { NotificationBusService } from './business/notification-bus.service';
import { Post } from './business/post.model';
import { Store } from './business/store';

@Injectable({ providedIn: 'root' })

export class PostsStore extends Store<Post[]>{

    constructor(private service: BackOfficePostService,
                private notify: NotificationBusService) {
        super();
    }

    init(): Promise<Post[]> {
        return this.service.getPostList().pipe(
            tap(this.store)
        ).toPromise();
    }

    newPost$(post: Post): Promise<Post> {
        return this.service.newPost(post).pipe(
            tap(postResult => {
                this.store([postResult, ...this.get()]);
            })).toPromise();
        }


    modifyPost$(postId: string, post: Post): Promise<Post> {
        return this.service.modifyPost(postId, post).pipe(
            tap(modifyPost => {
                const posts = this.get();
                const p = Object.assign({}, modifyPost);
                const index = this.searchIndex(posts, postId);
                const newPosts = [...posts.slice(0, index), p, ...posts.slice(index + 1)];
                this.store(newPosts);
            })
        ).toPromise();
    }

    deleteItemPost$(postId: string): Promise<Post> {
        return this.service.deleteItemPost(postId).pipe(
            tap(() => {
                const posts = this.get();
                const newPosts = posts.filter(post => post._id !== postId); this.store(newPosts);
            })).toPromise();
    }

    private searchIndex(posts: Post[], postId: string): number {
        return posts.findIndex(item => item._id === postId);
    }





}
