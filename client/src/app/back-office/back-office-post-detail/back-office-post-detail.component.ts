import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { NotificationBusService } from '../business/notification-bus.service';
import { Post } from '../business/post.model';
import { CommentsStore } from '../comments.store';

@Component({
  selector: 'app-back-office-post-detail',
  templateUrl: './back-office-post-detail.component.html',
  styleUrls: ['./back-office-post-detail.component.css']
})
export class BackOfficePostDetailComponent implements OnInit {

  displayModalComent: boolean;
  displayModalModifyComment: boolean;
  postById$: Observable<Post>;
  comment: Post;
  createComment: FormGroup;
  commentToModify;
  idExit: string;
  error;


  constructor(
    private activatedRoute: ActivatedRoute,
    private service: CommentsStore,
    private bus: NotificationBusService,
  ) { }

  ngOnInit(): void {
    const id = this.activatedRoute.snapshot.params.id;
    this.service.init(id);
    this.postById$ = this.service.get$();
    this.createComment = new FormGroup({
      nickName: new FormControl(''),
      comment: new FormControl('')
    });
  }

  ShowdisplayModalComent() {
    this.displayModalComent = true;
    this.idExit = this.activatedRoute.snapshot.params.id;
  }

  ShowdisplayModalModifyComment(comment: Post) {
    this.commentToModify = comment;
    this.displayModalModifyComment = true;
  }

  showCloseCreateCommnet(data: boolean) {
    this.displayModalComent = data;
  }

  showCloseModifyComment(data: boolean) {
    this.displayModalModifyComment  = data;
  }


  deleteItemComment(item) {
    this.service.deleteItemComment$(item._id)
    .then(() => {
      this.bus.showSuccess('Se ha eliminado correctamente este comentario con exito');
    })
    .catch(
      err => {
        this.error = err.error.message;
        this.bus.showError('No se ha podido eliminar este comentario no estas autorizado');
      });
  }



}
