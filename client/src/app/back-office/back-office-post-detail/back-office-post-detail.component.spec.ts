import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficePostDetailComponent } from './back-office-post-detail.component';

describe('BackOfficePostDetailComponent', () => {
  let component: BackOfficePostDetailComponent;
  let fixture: ComponentFixture<BackOfficePostDetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficePostDetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficePostDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
