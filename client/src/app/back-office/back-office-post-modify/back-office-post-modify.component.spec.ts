import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficePostModifyComponent } from './back-office-post-modify.component';

describe('BackOfficePostModifyComponent', () => {
  let component: BackOfficePostModifyComponent;
  let fixture: ComponentFixture<BackOfficePostModifyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficePostModifyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficePostModifyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
