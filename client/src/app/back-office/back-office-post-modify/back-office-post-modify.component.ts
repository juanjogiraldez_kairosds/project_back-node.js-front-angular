import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationBusService } from '../business/notification-bus.service';
import { PostsStore } from '../posts.store';


@Component({
  selector: 'app-back-office-post-modify',
  templateUrl: './back-office-post-modify.component.html',
  styleUrls: ['./back-office-post-modify.component.css']
})
export class BackOfficePostModifyComponent implements OnInit {

  @Input() entrada: boolean;
  @Input() dataPost: any;
  @Output() salida = new EventEmitter<boolean>();

  modifyPost: FormGroup;
  modifyErrorsMessages;
  error;

  constructor(private service: PostsStore,
              private bus: NotificationBusService) { }

  ngOnInit(): void {

    this.modifyPost = new FormGroup({
      author: new FormControl('', [Validators.required]),
      nickName: new FormControl('', [Validators.required]),
      title: new FormControl('', [Validators.required]),
      text: new FormControl('', [Validators.required])
    });
    this.modifyErrorsMessages = {
      required: 'Este campo es requerido'
    };
  }

  modify(id: string) {
  const post = this.modifyPost.value;
  this.service.modifyPost$(id, post)
  .then(() => {
    this.bus.showSuccess('Se ha modificado esta entrada de Post con éxito');
  })
  .catch(
    err => {
      this.error = err.error.message;
      this.bus.showError('No se ha podido modificar esta entrada de post , No te esta permitido modificar');
    });
  this.salida.emit(false);
  }

  showCloseThisModal() {
    this.modifyPost.reset();
    this.salida.emit(false);
  }

}
