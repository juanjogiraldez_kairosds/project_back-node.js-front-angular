import { TestBed } from '@angular/core/testing';

import { BackOfficePostService } from './back-office-post.service';

describe('BackOfficePostService', () => {
  let service: BackOfficePostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackOfficePostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
