import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { CommentDTO, PostDTO } from '../../models-dto/models-dto';
import { BackOfficePostProxyService } from './back-office-post-proxy.service';
import { Comments, Post } from './post.model';


@Injectable({
  providedIn: 'root'
})
export class BackOfficePostService {

  constructor(private proxy: BackOfficePostProxyService) { }


  getPostList(): Observable<Post[]> {
    return this.proxy.getPostList().pipe(
      map((postsDTO: PostDTO[]) => {
        let posts: Post[] = [];
        postsDTO.map((PostsDTO) => {
          posts = [...posts, this.DTOToModelTransformationPost(PostsDTO)];
        });
        return posts;
      })
    );
  }//

  getPostById(id: string): Observable<Post> {
    return this.proxy.getPostById(id).pipe(
      map(post => this.DTOToModelTransformationPost(post)))
      ;
  }//

  newPost(body: Post): Observable<Post> {
    return this.proxy.newPost(this.modelToDTOTransformationPost(body)).pipe(map(
      (postDTO: PostDTO) => {
        return {
          ...postDTO
        };
      }));
  }

  modifyPost(id: string, post: Post): Observable<Post> {
    return this.proxy.modifyPost(id, this.modelToDTOTransformationPost(post)).pipe(
      map(postDTO => this.DTOToModelTransformationPost(postDTO))
    );
  }

  deleteItemPost(id: string): Observable<Post> {
    return this.proxy.deleteItemPost(id).pipe(
      map(postDTO => this.DTOToModelTransformationPost(postDTO)));

  }

  newComment(id: string, body: Comments): Observable<CommentDTO> {
    return this.proxy.newComment(id, this.modelToDTOTransformationComments(body)).pipe(
      map((commentDTO: CommentDTO) => commentDTO));
  }

  modifyComment(id: string, comment: Comments): Observable<CommentDTO> {

    return this.proxy.modifyComment(id, this.modelToDTOTransformationComments(comment)).pipe(
      map(commentDTO => this.DTOToModelTransformationComments(commentDTO)));

  }

  deleteItemComment(id: string): Observable<CommentDTO> {
    return this.proxy.deleteItemComment(id).pipe(
      map(commentDTO => this.DTOToModelTransformationComments(commentDTO))
    )
      ;
  }

  private DTOToModelTransformationPost(postDTO: PostDTO): Post {
    return {
      _id: postDTO._id,
      author: postDTO.author,
      nickName: postDTO.nickName,
      title: postDTO.title,
      text: postDTO.text,
      comments: postDTO.comments
    };
  }

  private modelToDTOTransformationPost(post: Post): PostDTO {
    return {
      _id: post._id,
      author: post.author,
      nickName: post.nickName,
      title: post.title,
      text: post.text,
      comments: post.comments
    };
  }

  private DTOToModelTransformationComments(commentDTO: CommentDTO): Comments {
    console.log('estos es el commentDTO', commentDTO);
    return {
      _id: commentDTO._id,
      nickName: commentDTO.nickName,
      comment: commentDTO.comment,
      createdAt: commentDTO.createdAt
    };
  }
  private modelToDTOTransformationComments(comment: Comments): CommentDTO {
    return {
      _id: comment._id,
      nickName: comment.nickName,
      comment: comment.comment,
      createdAt: comment.createdAt
    };
  }
}



