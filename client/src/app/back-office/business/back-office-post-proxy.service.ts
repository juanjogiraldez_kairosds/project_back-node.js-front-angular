import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { CommentDTO, PostDTO } from '../../models-dto/models-dto';
import { Comments, Post } from './post.model';



@Injectable({
  providedIn: 'root'
})
export class BackOfficePostProxyService {


  constructor(private httpClient: HttpClient) { }

  getPostList(): Observable<PostDTO[]>{ //
    return this.httpClient.get<PostDTO[]>('http://localhost:3000/api/post');
  }

  getPostById(id: string): Observable<PostDTO> {//
    return this.httpClient.get<PostDTO>(`http://localhost:3000/api/post/${id}`);

  }
  newPost(body: Post): Observable<PostDTO> {//
    return this.httpClient.post<PostDTO>('http://localhost:3000/api/post', body);
  }
  modifyPost(id: string, post: Post): Observable<PostDTO> {
    return this.httpClient.put<PostDTO>(`http://localhost:3000/api/post/${id}`, post);
  }
  deleteItemPost(id: string): Observable<PostDTO> {
    return this.httpClient.delete<PostDTO>(`http://localhost:3000/api/post/${id}`);
  }
  newComment(id: string, body: Comments ): Observable<CommentDTO> {
    return this.httpClient.put<CommentDTO>(`http://localhost:3000/api/post/${id}/comment`, body) ;
  }
  modifyComment(id: string, comment ): Observable<CommentDTO> {
    return this.httpClient.put<CommentDTO>(`http://localhost:3000/api/comment/${id}`, comment);
  }
  deleteItemComment(id: string): Observable<CommentDTO> {
    return this.httpClient.delete<CommentDTO>(`http://localhost:3000/api/comment/${id}`);
  }


}

