import { TestBed } from '@angular/core/testing';

import { BackOfficePostProxyService } from './back-office-post-proxy.service';

describe('BackOfficePostProxyService', () => {
  let service: BackOfficePostProxyService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BackOfficePostProxyService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
