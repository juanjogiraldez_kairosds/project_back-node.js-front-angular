export interface Post {
    _id?: any;
    author?: string;
    nickName?: string;
    title?: string;
    text?: string;
    postIdAuthor?: string;
    createdAt?: Date;
    updatedAt?: Date;
    comments?: Comments[];
}

export interface Comments {
    _id?: string;
    nickName?: string;
    comment?: string;
    createdAt?: Date;

}


