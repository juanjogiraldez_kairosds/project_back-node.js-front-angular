import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationBusService } from '../business/notification-bus.service';
import { CommentsStore } from '../comments.store';

@Component({
  selector: 'app-back-office-create-comment',
  templateUrl: './back-office-create-comment.component.html',
  styleUrls: ['./back-office-create-comment.component.css']
})
export class BackOfficeCreateCommentComponent implements OnInit {

  @Input() entrada: boolean;
  @Input() idInput: string;
  @Output() salida = new EventEmitter<boolean>();

  createComment: FormGroup;
  commentErrorMessages;
  error;

  constructor(
    private createService: CommentsStore,
    private bus: NotificationBusService
  ) { }

  ngOnInit(): void {
    console.log(this.entrada);
    this.createComment = new FormGroup({
      nickName: new FormControl('', [Validators.required]),
      comment: new FormControl('', [Validators.required])
    });
    this.commentErrorMessages = {
      required: 'Este campo es requerido'
    };
  }
  showCloseThisModal() {
    this.createComment.reset();
    this.salida.emit(false);
  }

  newComment() {
    const body = this.createComment.value;
    const id = this.idInput;
    this.createService.newComment$(id, body)
      .then(() => {
        this.bus.showSuccess('Se ha creado un nuevo comentario');
      })
      .catch(
        err => {
          this.error = err.error.message;
          this.bus.showError('No se ha podido crear un comentario', 'Error: ');
        });
    this.createComment.reset();
    this.salida.emit(false);
  }


}


