import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BackOfficeCreateCommentComponent } from './back-office-create-comment.component';

describe('BackOfficeCreateCommentComponent', () => {
  let component: BackOfficeCreateCommentComponent;
  let fixture: ComponentFixture<BackOfficeCreateCommentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BackOfficeCreateCommentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BackOfficeCreateCommentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
