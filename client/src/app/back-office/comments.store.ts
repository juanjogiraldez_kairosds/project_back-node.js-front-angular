import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';
import { BackOfficePostService } from './business/back-office-post.service';
import { Comments, Post } from './business/post.model';
import { Store } from './business/store';

@Injectable({ providedIn: 'root' })

export class CommentsStore extends Store<Post>{

    constructor(private service: BackOfficePostService) {
        super();
    }

    init(id): Promise<Post> {
        return this.service.getPostById(id).pipe(
            tap(this.store)
        ).toPromise();
    }

    newComment$(id: string, comment: Comments): Promise<Comments> {
        return this.service.newComment(id, comment).pipe(
            tap(newComment => {
                const post = this.get();
                const newComments = [...post.comments, newComment];
                const newPost = { ...post, comments: newComments };
                this.store(newPost);
            })).toPromise();
    }

    deleteItemComment$(id: string): Promise<Comments> {
        return this.service.deleteItemComment(id).pipe(
            tap(() => {
                const post = this.get();
                const newPosts = post.comments.filter(comment => comment._id !== id);
                const newPost = { ...post, comments: newPosts };
                this.store(newPost);
            })
        ).toPromise();
    }

    modifyComment$(id: string, comment: Comments): Promise<Comments> {
        console.log('Estoy en el commentStore', id, comment);
        return this.service.modifyComment(id, comment).pipe(
            tap(newComment => {
                const post = this.get();
                const c = Object.assign({}, newComment);
                const index = this.searchIndex(post.comments, id);
                const newComments = [...post.comments.slice(0, index), c, ...post.comments.slice(index + 1)];
                const newPost = { ...post, comments: newComments };
                this.store(newPost);
            })
        ).toPromise();
    }

    private searchIndex(comments: Comments[], commentId: string): number {
        return comments.findIndex(item => item._id === commentId);
    }

}
