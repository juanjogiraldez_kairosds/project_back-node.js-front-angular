import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MessageModule } from 'primeng/message';
import { MessagesModule } from 'primeng/messages';
import { AccessModule } from './access/access.module';
import { AppComponent } from './app.component';
import { AuthInterceptorService } from './auth-interceptor.service';
import { BackOfficeModule } from './back-office/back-office.module';
import { HomeViewComponent } from './homepage/home-view/home-view.component';
import { HomepageModule } from './homepage/homepage.module';




const ROUTES: Routes = [
  { path: '', redirectTo: 'home-main', pathMatch: 'full' },
  { path: 'home-main', component: HomeViewComponent },
  {
    path: 'access-main',
    loadChildren: () => import('./access/access.module').then(m => m.AccessModule)
  },
  {
    path: 'BackOffice',
    loadChildren: () => import('./back-office/back-office.module').then(m => m.BackOfficeModule)
  },
  { path: '**', redirectTo: 'home-main' }
];

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    HomepageModule,
    BackOfficeModule,
    AccessModule,
    MessagesModule,
    MessageModule,
    RouterModule.forRoot(ROUTES)

  ],

  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptorService,
      multi: true
    }
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }
