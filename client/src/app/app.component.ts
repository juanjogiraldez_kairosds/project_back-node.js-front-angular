import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Notificacion } from '../app/back-office/business/notificaction';
import { NotificationBusService } from './back-office/business/notification-bus.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {


  constructor(private router: Router, private bus: NotificationBusService) { }
  title = 'client';


  msgs: Notificacion[] = [];


  ngOnInit(){
    console.log('arranca  AppComponent');
    this.bus.getNotificacion().subscribe(
      (notificacion) => {
        console.log('-------Notificación-------', notificacion);
        this.msgs = [];
        console.log('estos son los msgs', this.msgs);
        this.msgs.push(notificacion);
      }
    );
  }

  logout() {
    localStorage.removeItem('token');
    console.log('Has cerrado la sesión');
    // this.router.navigate(['home-main']);
  }



}
