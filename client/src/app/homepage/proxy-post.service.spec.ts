import { TestBed } from '@angular/core/testing';

import { ProxyPostService } from './proxy-post.service';

describe('ProxyPostService', () => {
  let service: ProxyPostService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ProxyPostService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
