import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { DetailDTO } from './detail-dto';
import { PostDTO } from './post-dto';
import { ProxyPostService } from './proxy-post.service';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private proxy: ProxyPostService) { }

  getPostList(): Observable<PostDTO[]> { return this.proxy.getPostList(); }

  getPostById(id: string ): Observable<DetailDTO> { return this.proxy.getPostById(id); }
}


