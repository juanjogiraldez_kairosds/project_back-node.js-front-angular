import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeMainComponent } from './home-main/home-main.component';
import { HomeViewComponent } from './home-view/home-view.component';
import { PostDetailComponent } from './posts/post-detail/post-detail.component';
import { PostComponent } from './posts/post/post.component';
import { PostsComponent } from './posts/posts.component';

const ROUTES: Routes = [
  {
    path: '', component: HomeViewComponent,
    children: [{ path: 'home-main', component: PostsComponent },
    { path: 'home-main/post/:id', component: PostDetailComponent }],
  }

];


@NgModule({
  declarations: [HomeViewComponent, PostComponent, PostDetailComponent, HomeMainComponent, PostsComponent],
  imports: [
    CommonModule,
    HttpClientModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [
    HomeViewComponent, PostComponent, PostDetailComponent, HomeMainComponent, PostsComponent
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class HomepageModule { }
