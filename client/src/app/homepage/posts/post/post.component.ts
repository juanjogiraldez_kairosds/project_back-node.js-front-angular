import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/internal/Observable';
import { Post } from 'src/app/back-office/business/post.model';
import { PostsStore } from 'src/app/back-office/posts.store';



@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent implements OnInit {

  postList$: Observable<Post[]>;

  constructor(private postService: PostsStore,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit(): void {
    this.postService.init();
    this.postList$ = this.postService.get$();
  }
  navegarHacia(id: string){
//     ev.preventDefault(),
console.log('recibo algo', `post/${id}` );

this.router.navigate([`post/${id}`], {relativeTo: this.route});
//
  }
  // handleSubmit = (ev: Event) => {
  //   ev.preventDefault();
  //   console.log(this.textInput.value);
  // }


}


