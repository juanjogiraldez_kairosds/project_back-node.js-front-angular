import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { BackOfficePostService } from 'src/app/back-office/business/back-office-post.service';
import { Post } from 'src/app/back-office/business/post.model';


@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {

  postById$: Observable<Post>;

  constructor(private activatedRoute: ActivatedRoute, private postService: BackOfficePostService) { }

  ngOnInit(): void{

    const id = this.activatedRoute.snapshot.params.id;
    this.postById$ = this.postService.getPostById(id);
  }

}
