'use strict'

const express = require('express');
const connectedDb = require('./mongoDb/connection.js');
const server = require('./server.js');
const configuration = require('./configuration.js');
const routes = require('./routes.js');
const auth = require('./auth.js')

const app = express();

connectedDb();
server(app);
configuration(app);
routes(app);
auth(app);


module.exports = app;
