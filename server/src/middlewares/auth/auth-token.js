'use strict'

const jwt = require("jsonwebtoken");
const SECRET_KEY = "SECRET_KEY";

module.exports = (req, res) => {
    
    const body = { _id: req.user._id, userName: req.user.user, role: req.user.role };

    // const opts = { expiresIn: 120}; //token expires in 2min // permanenncia de del token en segundos 
    const token = jwt.sign({ body }, SECRET_KEY); // , opts
    
    return res.status(200).json({ message: "Auth Passed", token });

};