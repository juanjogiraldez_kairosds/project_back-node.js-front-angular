'use strict'

const  userRepository= require('../items/user/repository.js')
const  bcrypt = require('bcrypt');


module.exports =  async function verify (username, password, done ) {
    // console.log('------', username, password)
    const user = await userRepository.getFindOne({user: username});
    
    const verifyPassword = async function (user, password) {
        return await bcrypt.compare(password, user.passwordHash)};

    if(!user){
        return done(null, false, { message: 'User not found' });
    }
    if(await verifyPassword(user, password)){
        return done(null, user);
    } else {
        return done(null, false, { message: 'Incorrect password' });
    }


}

