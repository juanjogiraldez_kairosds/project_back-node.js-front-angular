'use strict'

const OffensiveWordRepository = require('../items/offensiveWord/repository.js');

module.exports =  async () => {
    try {
        const offensiveWords = await OffensiveWordRepository.getAll() ;
        if (offensiveWords.length === 0) {
            OffensiveWordRepository.addOffensiveWord({ word: 'Caca', level: 3 });
            OffensiveWordRepository.addOffensiveWord({ word: 'Culo', level: 2 });
            OffensiveWordRepository.addOffensiveWord({ word: 'Cabron', level: 5 });
            OffensiveWordRepository.addOffensiveWord({ word: 'Pipi', level: 1 });
            OffensiveWordRepository.addOffensiveWord({ word: 'Gilipollas', level: 4 });
            console.info(' Offensive words basics ');
        }
    } catch (err) {
        console.log(err);
    }
}