'use strict'

const CommentController = require('../items/comment/controller.js');
const OffensiveValidator = require('../middlewares/validator.js')
const passport = require('passport');
const express = require('express');
const comment = express.Router();
const UserControl = require('../middlewares/userControl.js')

comment.get('/', CommentController.getAllComments)
comment.put('/:id', passport.authenticate('jwt', { session: false }), UserControl.updateOnwComment, OffensiveValidator.checkwords, CommentController.updateComment);
comment.delete('/:id', passport.authenticate('jwt', { session: false }), UserControl.deleteComment , CommentController.deleteComment );


module.exports = comment