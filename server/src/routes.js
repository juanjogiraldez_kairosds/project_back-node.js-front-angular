'use strict'

const Post = require('./routes/post.routes.js');
const Comment = require('./routes/comment.routes.js');
const OfensiveWord = require('./routes/offensiveWord.routes.js');
const User = require('./routes/user.routes.js')
const passport = require('passport');
const morgan = require('morgan')
const cors = require('cors');
const token = require('./middlewares/auth/auth-token.js');

module.exports = app => { 
    
    app.use(cors());
    app.use(morgan('dev'));
    app.use('/api/comment', Comment);
    app.use('/api/post', Post);
    app.use('/api/ofensiveWord', OfensiveWord);
    app.use('/api/user', User);
    app.post('/api/login', passport.authenticate('basic', { session: false }), token );
    
}