'use strict'

const CheckOffensiveValidator = require('../validators/check-word.js')

describe('Check offensive word', () => {

    it ( 'should not include an offensive word', () => {

        const sentence = 'tienes la cara de Porcelana';
        const offensiveWord = [{word: 'Calientapollas' , level: 4}];
    
        const hadOffensiveWord = CheckOffensiveValidator.check(sentence, offensiveWord)
    
        expect(hadOffensiveWord.length).toBe(0)
    });
    
    it ( 'should include an offensive word', () => {
    
        const sentence = 'tienes la cara de  Calientapollas';
        const offensiveWord = [{word: 'Calientapollas' , level: 4}];
    
        const hadOffensiveWord = CheckOffensiveValidator.check(sentence, offensiveWord)
    
        expect(hadOffensiveWord.length).toBe(1)
    });


})

