'use strict'

const UserSchema = require('./model.js');
var bcrypt = require('bcrypt');
const UserRepository = {};

UserRepository.getFind = async (e) => {
    return await UserSchema.find(e)//.exec();
};
UserRepository.getFindOne = async (e) => {
    return await UserSchema.findOne(e).exec();
};

UserRepository.addUser = async (user,passwordHash,role) => {
    
    const passwdHash = await bcrypt.hash(passwordHash, bcrypt.genSaltSync(8), null);
    const users = new UserSchema({user, passwordHash: passwdHash, role});
    return await users.save();


};

module.exports = UserRepository