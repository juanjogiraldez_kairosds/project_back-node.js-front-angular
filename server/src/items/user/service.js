'use strict'

const UserRepository = require('./repository.js');


const UserService= {}

UserService.getFind = async () => {
    return await UserRepository.getFind();
}
UserService.getFindOne = async () => {
        return await UserRepository.getFindOne();
    }

UserService.addUser = async (user,passwordHash,role) => {
        return await  UserRepository.addUser(user,passwordHash,role);
    }

module.exports = UserService;