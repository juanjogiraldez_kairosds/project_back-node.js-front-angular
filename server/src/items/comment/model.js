'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CommentsSchema = Schema({
    nickName: {type: "String"},
    comment: {type: "String"}, 
    date: { type: "Date", default:  Date.now},
    postIdNickName: { type: "String"}, 
    authorPostId: { type: "String"},
    ownerAuthorId: { type: "String"}
},{
    timestamps: true
});

module.exports = mongoose.model('comments', CommentsSchema)
