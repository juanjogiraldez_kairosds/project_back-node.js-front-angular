'use strict'

const CommentRepository = require('../comment/repository.js')
const CommentsService = require('./service.js')

const CommentController = {};

CommentController.getAllComments = async (req, res, next) => {
    try {
        const allComments = await CommentsService.getAllComments();
        res.status(200).json(allComments)
    } catch (error){
        console.log(error)
    }
}

CommentController.updateComment =  async (req, res, next) => {  
     try {
            const { id }= req.params
            const comment = req.body;
            const postUpdate = await CommentsService.updateComment(id, comment);
            res.status(200).json(postUpdate); 
        } catch (error) { 
            res.json(error)
        };
    };
    


    
CommentController.deleteComment = async(req, res, next) => {
    try { 
            const idComment = req.params.id
        
            const result = await CommentsService.deleteComment(idComment);
            res.status(200).send( { message: "you are allowed to delete" })
                    
       } catch (error) {
        console.log(error)
    }
};

module.exports = CommentController

