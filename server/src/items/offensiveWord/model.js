'use strict'

const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const OffensiveWordSchema = Schema({
    word: {type: "String"},
    level: Number, 
}, {
    timestamps: true
});

module.exports = mongoose.model('offensiveWords', OffensiveWordSchema)