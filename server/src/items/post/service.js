'use strict'

const PostRepository = require('./repository.js')

const PostService= {}


// añadir try/catch y añadir codigo de errores , ver ejemplo proyecto Ruben 
PostService.getAll = async () => {
        return await PostRepository.getAll();
    }

    PostService.getById = async (id) => {
        const post = await PostRepository.getById(id);
        return post;
    }
    // PostService.getAllEntryAuthor = async (userId) => {
    //     return await PostRepository.getAllEntryAuthor(userId)
    // }

    PostService.addPost = async (body, userId) => {
        return await  PostRepository.addPost(body, userId);
    }

    PostService.updatePost = async (id, post) => {
        return await PostRepository.updatePost(id, post);
    }

    PostService.deletePost = async (id, post) => {
        
        return await PostRepository.deletePost(id, post);
    } 

    PostService.addComment = async (idPost, comment, postIdNickName, owneAuthorId) => {
        return await PostRepository.addComment(idPost, comment, postIdNickName, owneAuthorId);
    }



module.exports = PostService;