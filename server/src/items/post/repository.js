'use strict'

const PostSchema = require('./model.js');
const CommentRepository = require('../comment/repository.js');
const PostRepository = {}

PostRepository.getAll = async () => {
        return await PostSchema.find().select({comments: 0, __v:0});
    }

    PostRepository.getById = async (id) => {
        const post = await PostSchema.findById(id).populate('comments');
        return post;
    }

    PostRepository.addPost = async (body, userId) => {

        const nwPst = new PostSchema({ author: body.author,nickName: body.nickName, title: body.title, text: body.text, postIdAuthor: userId} );
        return await nwPst.save();
    }

    PostRepository.updatePost = async (id, post) => {
        let update = await PostSchema.findByIdAndUpdate(id, post, {new: true});
        return update;
    }

    PostRepository.deletePost = async (id, post) => {
        let deletePost = await PostSchema.findByIdAndDelete(id, post);
        return deletePost;
    } 

    PostRepository.addComment = async (idPost, comment, postIdNickName, owneAuthorId) => {
        const nwCmmnt = await CommentRepository.addComment(idPost, comment, postIdNickName, owneAuthorId);
        let update = await PostSchema.findByIdAndUpdate(idPost, {$push: {comments: nwCmmnt}}, {new: true});
        return update;
    }




module.exports = PostRepository;