import { newE2EPage } from '@stencil/core/testing';

describe('my-layoutflexbox', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<my-layoutflexbox></my-layoutflexbox>');

    const element = await page.find('my-layoutflexbox');
    expect(element).toHaveClass('hydrated');
  });
});
