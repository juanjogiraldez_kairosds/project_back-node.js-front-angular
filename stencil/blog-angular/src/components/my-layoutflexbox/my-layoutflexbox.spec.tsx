import { newSpecPage } from '@stencil/core/testing';
import { MyLayoutflexbox } from './my-layoutflexbox';

describe('my-layoutflexbox', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [MyLayoutflexbox],
      html: `<my-layoutflexbox></my-layoutflexbox>`,
    });
    expect(page.root).toEqualHtml(`
      <my-layoutflexbox>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </my-layoutflexbox>
    `);
  });
});
