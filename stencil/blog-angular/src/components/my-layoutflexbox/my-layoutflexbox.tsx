import { Component, ComponentInterface, h } from '@stencil/core';

@Component({
  tag: 'my-layoutflexbox',
  styleUrl: 'my-layoutflexbox.css',
  shadow: true,
})
export class MyLayoutflexbox implements ComponentInterface {

  render() {
    return (
      <body>
        <nav class="top-bar">
            <slot name="nav"/>
          </nav>
        <section>
          <div class="container">
            <slot name="section" />
          </div>
        </section>
        <footer>
          <div>
            <slot name="footer" />
          </div>
        </footer>

      </body>

    );
  }

}
