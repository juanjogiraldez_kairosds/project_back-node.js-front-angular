import { newSpecPage } from '@stencil/core/testing';
import { MyCardDetail } from './my-card-detail';

describe('my-card-detail', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [MyCardDetail],
      html: `<my-card-detail></my-card-detail>`,
    });
    expect(page.root).toEqualHtml(`
      <my-card-detail>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </my-card-detail>
    `);
  });
});
