# my-card-detail



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type     | Default |
| ---------- | ----------- | ----------- | -------- | ------- |
| `date`     | `date`      |             | `string` | `""`    |
| `nickName` | `nick-name` |             | `string` | `""`    |
| `text`     | `text`      |             | `string` | `""`    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
