import { newE2EPage } from '@stencil/core/testing';

describe('my-card-detail', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<my-card-detail></my-card-detail>');

    const element = await page.find('my-card-detail');
    expect(element).toHaveClass('hydrated');
  });
});
