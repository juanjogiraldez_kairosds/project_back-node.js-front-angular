import { Component, ComponentInterface, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-card-detail',
  styleUrl: 'my-card-detail.css',
  shadow: true,
})
export class MyCardDetail implements ComponentInterface {

  @Prop() nickName: string = "";
  @Prop() date: string = "";
  @Prop() text: string = "";

  render() {
    return (
      <main>
        <div>
          <div class="card">
            <div class="card-content">
              <h4 class="card-full-Title" >{this.nickName}</h4>
              <p>{this.text}</p>
              <h4>{this.date}</h4>
            </div>
          </div>
        </div>
      </main>
    );
  }

}

