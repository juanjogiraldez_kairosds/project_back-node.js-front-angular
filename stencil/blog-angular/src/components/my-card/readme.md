# my-card



<!-- Auto Generated Below -->


## Properties

| Property   | Attribute   | Description | Type     | Default |
| ---------- | ----------- | ----------- | -------- | ------- |
| `date`     | `date`      |             | `string` | `""`    |
| `nickName` | `nick-name` |             | `string` | `""`    |
| `text`     | `text`      |             | `string` | `""`    |
| `titulo`   | `titulo`    |             | `string` | `""`    |


## Events

| Event    | Description | Type               |
| -------- | ----------- | ------------------ |
| `enlace` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
