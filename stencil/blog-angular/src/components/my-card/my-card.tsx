import { Component, ComponentInterface, Event, EventEmitter, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-card',
  styleUrl: 'my-card.css',
  shadow: true,
})
export class MyCard implements ComponentInterface {

  @Prop() titulo: string = "";
  @Prop() nickName: string = "";
  @Prop() date: string = "";
  @Prop() text: string = "";
  @Event() enlace: EventEmitter;

  handleClick() {
    this.enlace.emit();
  }

  render() {
    return (
      <main>
        <div class="card-content">
          <div class="card">
            <div class="card-content">
              <h2 class="card-full-Title" >{this.titulo}</h2>
              <h3>de  <i>{this.nickName}</i></h3>
              <h4>{this.date}</h4>
              <p>{this.text}</p>
              {/* <slot name="intruso"></slot> */}
              <button onClick={() => this.handleClick()}>Leer más</button>
            </div>
          </div>
        </div>
      </main>
    );
  }

}

