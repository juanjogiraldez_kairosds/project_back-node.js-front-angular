import { Component, ComponentInterface, h } from '@stencil/core';

@Component({
  tag: 'my-login',
  styleUrl: 'my-login.css',
  shadow: true,
})
export class MyLogin implements ComponentInterface {

  render() {
    return (

      <div class="cont">

        <div class="form">
          <form action="">
            <h1>Login</h1>
            <input type="text"
              class="user"
              placeholder="Username"  />
            <input type="password"
              class="pass"
              placeholder="Password" />
            <button class="login">Login</button>
          </form>
        </div>

      </div>
    );
  }

}
