import { newE2EPage } from '@stencil/core/testing';

describe('my-postdetail', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<my-postdetail></my-postdetail>');

    const element = await page.find('my-postdetail');
    expect(element).toHaveClass('hydrated');
  });
});
