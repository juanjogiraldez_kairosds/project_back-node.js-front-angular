import { newSpecPage } from '@stencil/core/testing';
import { MyPostdetail } from './my-postdetail';

describe('my-postdetail', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [MyPostdetail],
      html: `<my-postdetail></my-postdetail>`,
    });
    expect(page.root).toEqualHtml(`
      <my-postdetail>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </my-postdetail>
    `);
  });
});
