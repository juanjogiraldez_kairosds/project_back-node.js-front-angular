import { Component, ComponentInterface, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-postdetail',
  styleUrl: 'my-postdetail.css',
  shadow: true,
})
export class MyPostdetail implements ComponentInterface {

  @Prop() titulo: string = "";
  @Prop() nickName: string = "";
  @Prop() date: string = "";
  @Prop() text: string = "";

 

  render() {
    return (
      <main>
        <div class="card-content">
          <div class="card">
            <div class="card-content">
              <h2 class="card-full-Title" >{this.titulo}</h2>
              <h3>de  <i>{this.nickName}</i></h3>
              <h4>{this.date}</h4>
              <p>{this.text}</p>
            </div>
          </div>
        </div>
      </main>
    );
  }

}