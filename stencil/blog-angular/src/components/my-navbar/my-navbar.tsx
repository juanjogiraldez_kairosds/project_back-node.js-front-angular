import { Component, ComponentInterface, h, Prop } from '@stencil/core';

@Component({
  tag: 'my-navbar',
  styleUrl: 'my-navbar.css',
  shadow: true,
})
export class MyNavbar implements ComponentInterface {
  @Prop() titulo0: string = '';
  @Prop() titulo1: string = '';
  @Prop() titulo2: string = '';
  @Prop() titulo3: string = '';
  @Prop() titulo4: string = '';

  render() {
    return (
      <div>
        <nav >
          {/* <a class="toggle open" href="#nav">open</a>
        <a class="toggle close" href="#">×</a> */}
          <ul class="topnav"> 
            <li><a class="active" href="#home">{this.titulo1}</a></li>
            <li><a href="#news">{this.titulo2}</a></li>
            <li><a href="#contact">{this.titulo3}</a></li>
            <li class="right"><a href="#about">{this.titulo4}</a></li>
          </ul>
        </nav>
      </div>

    );
  }

}
