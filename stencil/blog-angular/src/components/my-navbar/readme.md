# my-navbar



<!-- Auto Generated Below -->


## Properties

| Property  | Attribute  | Description | Type     | Default |
| --------- | ---------- | ----------- | -------- | ------- |
| `titulo0` | `titulo-0` |             | `string` | `''`    |
| `titulo1` | `titulo-1` |             | `string` | `''`    |
| `titulo2` | `titulo-2` |             | `string` | `''`    |
| `titulo3` | `titulo-3` |             | `string` | `''`    |
| `titulo4` | `titulo-4` |             | `string` | `''`    |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
