import { Component, ComponentInterface, h } from '@stencil/core';

@Component({
  tag: 'my-layout',
  styleUrl: 'my-layout.css',
  shadow: true,
})
export class MyLayout implements ComponentInterface {

  render() {
    return (
      <main>
          <nav><slot name="nav" /></nav>
          <section ><slot name="section" /></section>
      </main>

    );
  }

}
